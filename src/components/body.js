import { Component } from "react";

class UpdownState extends Component{
    constructor(props){
        super(props);
        this.state = {
            count: 0
        }
    }
    onBtnUp =() =>{
        console.log("Click Button Up");
        this.setState({
            count: this.state.count + 1
        })
        console.log(this.state.count)
    }
    onBtnDown =() =>{
        console.log("Click Button Down");
        this.setState({
            count: this.state.count - 1
        })
        console.log(this.state.count)
    }
    render(){

        return(
            
            <div style={{display:"flex", marginTop:"5px",textAlign:"center", justifyContent:"center"}}>
                <div>
                    <div>
                        Count Number: {this.state.count}
                    </div>
                    <div>
                        <button style={{width:"50px",height:"30px",margin:"5px"}} onClick={this.onBtnUp}>↑</button>
                        <button style={{width:"50px",height:"30px",margin:"5px"}} onClick={this.onBtnDown}>↓</button>
                    </div>
                </div>
            </div>
        )
    }
}
export default UpdownState;